//
//  AppDelegate.swift
//  LocalServer
//
//  Created by Charisma Velasco on 10/12/15.
//  Copyright © 2015 Loveseat. All rights reserved.
//

import Cocoa
import Foundation

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {

    @IBOutlet weak var window: NSWindow!
    let task = NSTask()

    func applicationDidFinishLaunching(aNotification: NSNotification) {
        let txtField: NSTextField = NSTextField(frame: CGRect(x: 0, y: 0, width: 480, height: 360));
        self.window.contentView!.addSubview(txtField)
        
        task.launchPath = "\(NSHomeDirectory())/.virtualenvs/loveseat/bin/python"
        task.arguments = ["\(NSHomeDirectory())/Code/bidbash/manage.py", "runserver"]
        
        let pipe = NSPipe()
        task.standardOutput = pipe
        task.standardError = pipe
        
        let outHandle = pipe.fileHandleForReading
        outHandle.waitForDataInBackgroundAndNotify()
        
        var obs1 : NSObjectProtocol!
        obs1 = NSNotificationCenter.defaultCenter().addObserverForName(NSFileHandleDataAvailableNotification,
            object: outHandle, queue: nil) {  notification -> Void in
                let data = outHandle.availableData
                if data.length > 0 {
                    if let str = NSString(data: data, encoding: NSUTF8StringEncoding) {
                        // print("got output: \(str)")
                        txtField.stringValue += str as String
                    }
                    outHandle.waitForDataInBackgroundAndNotify()
                } else {
                    print("EOF on stdout from process")
                    NSNotificationCenter.defaultCenter().removeObserver(obs1)
                }
        }
        
        var obs2 : NSObjectProtocol!
        obs2 = NSNotificationCenter.defaultCenter().addObserverForName(NSTaskDidTerminateNotification,
            object: task, queue: nil) { notification -> Void in
                print("terminated")
                NSNotificationCenter.defaultCenter().removeObserver(obs2)
        }
        
        task.launch()
    }

    func applicationWillTerminate(aNotification: NSNotification) {
        task.terminate()
    }


}

